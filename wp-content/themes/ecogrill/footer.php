<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 */

?>

    </main>

    <footer>

    <div class="container">
        <div class="row align-items-center">

            <div class="col-12 col-lg-6">
                <div class="footer-copyrights">
	                <?php if ( get_field( 'footer_logo' ) ) { ?>
                        <div class="logo-small">
                            <img src="<?php the_field( 'footer_logo' ); ?>" alt="<?php the_field( 'footer_logo_alt' ); ?>">
                        </div>
	                <?php } ?>
	                <?php if ( get_field( 'footer_copyrights' ) ) { ?>
                        <div class="footer-copyrights-text"><?php the_field( 'footer_copyrights' ); ?></div>
	                <?php } ?>
                </div>
	            <?php if ( get_field( 'footer_copyrights_tagline' ) ) { ?>
                    <div class="footer-copyrights-tagline"><?php the_field( 'footer_copyrights_tagline' ); ?></div>
	            <?php } ?>
            </div>

            <div class="col-12 col-lg-6 text-lg-right">
	            <?php if ( get_field( 'header_address_text' ) ) { ?>
                    <div class="footer-address"><?php the_field( 'header_address_text' ) ?></div>
	            <?php } ?>

	            <?php if ( get_field( 'phone_number' ) ) { ?>
                    <div class="footer-phone">телефон <a href="tel:<?php echo preg_replace("/[^\d+]/", '', get_field('phone_number')); ?>"><?php the_field( 'phone_number' ); ?></a>,</div>
	            <?php } ?>
	            <?php if ( get_field( 'email_address' ) ) { ?>
                    <div class="footer-email">электронная почта <a href="mailto:<?php the_field( 'email_address' ); ?>"><?php the_field( 'email_address' ); ?></a></div>
	            <?php } ?>
            </div>

        </div>
    </div>

    <div class="footer-tagline">

        <div class="container">
            <div class="row align-items-center">
	            <?php if ( get_field( 'footer_tagline' ) ) { ?>
                    <div class="col-12 footer-tagline-wrapper"><?php the_field( 'footer_tagline' ); ?></div>
	            <?php } ?>
            </div>
        </div>

    </div>
</footer>

</div> <!-- end of wrapper -->

<div class="modal fade" id="callback" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-header">Обратный звонок</div>
            <div class="form-callback">
	            <?php echo do_shortcode('[contact-form-7 id="297" title="Form callback"]'); ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="properties" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-properties" role="document">
        <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-header"><?php the_field( 'properties_title' ) ?></div>
            <div class="properties-list">
                <?php
                $properties = get_field( 'properties' );
                if($properties) {
                    foreach ( $properties as $key => $value ) {
                        ?>
                        <div class="properties-item">
                            <div class="properties-title"><?php echo $value['title']; ?></div>
                            <div class="properties-value"><?php echo $value['value']; ?></div>
                        </div>
                        <?php
                    }
                }?>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="accessories-mail-sent-success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-header">
	            <?php if ( get_field( 'accessories_modal_title' ) ) { ?>
		            <?php the_field( 'accessories_modal_title' ); ?>
	            <?php } ?>
            </div>
            <p>
	            <?php if ( get_field( 'accessories_modal_text' ) ) { ?>
		            <?php the_field( 'accessories_modal_text' ); ?>
	            <?php } ?>
            </p>
        </div>
    </div>
</div>

<div class="modal fade" id="order-mail-sent-success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-header">
		        <?php if ( get_field( 'order_modal_title' ) ) { ?>
			        <?php the_field( 'order_modal_title' ); ?>
		        <?php } ?>
            </div>
            <p>
		        <?php if ( get_field( 'order_modal_text' ) ) { ?>
			        <?php the_field( 'order_modal_text' ); ?>
		        <?php } ?>
            </p>
        </div>
    </div>
</div>

<div class="modal fade" id="callback-mail-sent-success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-header">
		        <?php if ( get_field( 'callback_modal_title' ) ) { ?>
			        <?php the_field( 'callback_modal_title' ); ?>
		        <?php } ?>
            </div>
            <p>
		        <?php if ( get_field( 'callback_modal_text' ) ) { ?>
			        <?php the_field( 'callback_modal_text' ); ?>
		        <?php } ?>
            </p>
        </div>
    </div>
</div>

<div class="modal fade" id="top-mail-sent-success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-header">
		        <?php if ( get_field( 'top_modal_title' ) ) { ?>
			        <?php the_field( 'top_modal_title' ); ?>
		        <?php } ?>
            </div>
            <p>
		        <?php if ( get_field( 'top_modal_text' ) ) { ?>
			        <?php the_field( 'top_modal_text' ); ?>
		        <?php } ?>
            </p>
        </div>
    </div>
</div>

<?php wp_footer(); ?>
</body>

</html>