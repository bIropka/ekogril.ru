<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 */
?>
<?php get_header(); ?>

	<div class="top <?php the_field( 'top_layout' ); ?>">
		<div class="top-slider">
			<?php
			$slider_top = get_field( 'slider_top' );
			if($slider_top) {
				foreach ( $slider_top as $key => $value ) {
					?>
                    <div class="top-slide" style="background: url('<?php echo $value['image']; ?>') no-repeat center center;">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <div class="top-slider-tagline"><?php echo $value['text']; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
					<?php
				}
            }?>
		</div>
		<div class="form-gift">
			<?php if ( get_field( 'top_form_title' ) ) { ?>
                <div class="form-gift-title"><?php the_field( 'top_form_title' ); ?></div>
			<?php } ?>
			<?php if ( get_field( 'top_form_subtitle' ) ) { ?>
                <div class="form-gift-subtitle"><?php the_field( 'top_form_subtitle' ); ?></div>
			<?php } ?>
			<?php if ( get_field( 'top_form_image' ) ) { ?>
                <div class="form-gift-image"><img src="<?php the_field( 'top_form_image' ); ?>" alt="<?php the_field( 'top_form_image_alt' ); ?>"></div>
			<?php } ?>
			<?php if ( get_field( 'top_form_text' ) ) { ?>
                <p><?php the_field( 'top_form_text' ); ?></p>
			<?php } ?>
			<?php if ( get_field( 'top_form_cost' ) ) { ?>
                <div class="form-gift-offer"><?php the_field( 'top_form_cost' ); ?></div>
			<?php } ?>
			<?php echo do_shortcode('[contact-form-7 id="168" title="Form top"]'); ?>
		</div>
	</div>

	<div class="about" id="about">
		<div class="container">
			<div class="row">
				<div class="col-12 d-flex flex-wrap">
					<div class="about-content">
						<?php if ( get_field( 'about_title' ) ) { ?>
                            <h2 class="text-red"><?php the_field( 'about_title' ); ?></h2>
						<?php } ?>
						<?php if ( get_field( 'about_subtitle_1' ) ) { ?>
                            <h3><?php the_field( 'about_subtitle_1' ); ?></h3>
						<?php } ?>
						<?php if ( get_field( 'about_text_1' ) ) { ?>
                            <p><?php the_field( 'about_text_1' ); ?></p>
						<?php } ?>
						<?php if ( get_field( 'about_image_primary' ) ) { ?>
                            <div class="about-image"><img src="<?php the_field( 'about_image_primary' ); ?>" alt="<?php the_field( 'about_image_primary_alt' ); ?>"></div>
						<?php } ?>
						<div class="about-column">
							<?php if ( get_field( 'about_subtitle_2' ) ) { ?>
                                <h3><?php the_field( 'about_subtitle_2' ); ?></h3>
							<?php } ?>
							<?php if ( get_field( 'about_text_2' ) ) { ?>
                                <p><?php the_field( 'about_text_2' ); ?></p>
							<?php } ?>
							<?php
							$about_advantages = get_field( 'about_list_advantages' );
							if($about_advantages) {
								foreach ( $about_advantages as $key => $value ) {
									?>
                                    <ul class="custom-list">
                                        <li><?php echo $value['text']; ?></li>
                                    </ul>
									<?php
								}
							}?>
						</div>
					</div>
                    <div class="about-magic">
	                    <?php if ( get_field( 'about_image_secondary' ) ) { ?>
                            <div class="about-magic-image"><img src="<?php the_field( 'about_image_secondary' ); ?>" alt="<?php the_field( 'about_image_secondary_alt' ); ?>"></div>
	                    <?php } ?>
	                    <?php if ( get_field( 'about_subtitle_3' ) ) { ?>
                            <h3><?php the_field( 'about_subtitle_3' ); ?></h3>
	                    <?php } ?>
	                    <?php if ( get_field( 'about_text_3' ) ) { ?>
                            <p><?php the_field( 'about_text_3' ); ?></p>
	                    <?php } ?>
	                    <?php if ( get_field( 'about_subtitle_4' ) ) { ?>
                            <h3><?php the_field( 'about_subtitle_4' ); ?></h3>
	                    <?php } ?>
	                    <?php if ( get_field( 'about_text_4' ) ) { ?>
                            <p><?php the_field( 'about_text_4' ); ?></p>
	                    <?php } ?>
	                    <?php
	                    $about_components = get_field( 'about_list_components' );
	                    if($about_components) {
	                        ?>
                            <div class="about-magic-list">
			                    <?php
			                    foreach ( $about_components as $key => $value ) {
				                    ?>
                                    <div class="about-magic-item">
                                        <div class="about-magic-icon">
                                            <img src="<?php echo $value['image']; ?>" alt="<?php echo $value['image_alt']; ?>">
                                        </div>
                                        <div class="about-magic-text"><?php echo $value['text']; ?></div>
                                    </div>
				                    <?php
			                    } ?>
                            </div>
                            <?php
	                    }?>
                    </div>
				</div>
			</div>
		</div>
	</div>

	<div class="trivia" style="background: url('<?php the_field( 'trivia_background' ); ?>') no-repeat center center; background-size: cover;">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php if ( get_field( 'trivia_title' ) ) { ?>
                        <h2><?php the_field( 'trivia_title' ); ?></h2>
					<?php } ?>
					<ul class="trivia-list">
						<?php
						$trivia_items = get_field( 'trivia_items' );
						if($trivia_items) {
							foreach ( $trivia_items as $key => $value ) {
								?>
                                <li class="trivia-item"><?php echo $value['text']; ?></li>
								<?php
							}
						}?>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="ways" id="ways" style="background: url('<?php the_field( 'ways_background' ); ?>') no-repeat center center; background-size: cover;">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php if ( get_field( 'ways_title' ) ) { ?>
                        <h2><?php the_field( 'ways_title' ); ?></h2>
					<?php } ?>
					<?php if ( get_field( 'ways_title_label' ) ) { ?>
                        <div class="title-marker">
                            <div class="title-marker-text"><?php the_field( 'ways_title_label' ); ?></div>
                        </div>
					<?php } ?>
					<ul class="ways-list">
						<?php
						$ways_item = get_field( 'ways_item' );
						if($ways_item) {
							foreach ( $ways_item as $key => $value ) {
								?>
                                <li class="ways-item">
                                    <div class="ways-icon"><img src="<?php echo $value['icon']; ?>" alt="<?php echo $value['icon_alt']; ?>"></div>
                                    <div class="ways-title text-red"><?php echo $value['title']; ?></div>
                                    <div class="ways-subtitle"><?php echo $value['subtitle']; ?></div>
                                    <div class="ways-text"><?php echo $value['text']; ?></div>
                                </li>
								<?php
							}
						}?>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="order">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php if ( get_field( 'order_title_top' ) ) { ?>
                        <h2><?php the_field( 'order_title_top' ); ?></h2>
					<?php } ?>
					<?php if ( get_field( 'order_image' ) ) { ?>
                        <div class="order-image"><img src="<?php the_field( 'order_image' ); ?>" alt="<?php the_field( 'order_image_alt' ); ?>"></div>
					<?php } ?>
					<?php echo do_shortcode('[contact-form-7 id="225" title="Form order top"]'); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="advantages" id="advantages">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php if ( get_field( 'advantages_title' ) ) { ?>
                        <h2 class="text-red"><?php the_field( 'advantages_title' ); ?></h2>
					<?php } ?>
					<div class="advantages-list">
						<?php
						$advantages_item = get_field( 'advantages_item' );
						if($advantages_item) {
							foreach ( $advantages_item as $key => $value ) {
								?>
                                <div class="advantages-item">
                                    <div class="advantages-image"><img src="<?php echo $value['image']; ?>" alt="<?php echo $value['image_alt']; ?>"></div>
                                    <div class="advantages-title"><?php echo $value['title']; ?></div>
                                    <div class="advantages-subitem">
                                        <div class="advantages-close"></div>
                                        <div class="advantages-subtitle"></div>
                                        <div class="advantages-subimage"></div>
                                        <div class="advantages-subtext"><?php echo $value['text']; ?></div>
                                    </div>
                                </div>
								<?php
							}
						}?>
					</div>
				</div>
			</div>
		</div>
	</div>

    <div class="form-accessories" style="background: url('<?php the_field( 'accessories_background' ); ?>') no-repeat center center; background-size: cover;">
        <div class="container">
            <div class="row">
                <div class="col-12">
				    <?php if ( get_field( 'accessories_title' ) ) { ?>
                        <h2><?php the_field( 'accessories_title' ); ?></h2>
				    <?php } ?>
                </div>
            </div>
	        <?php echo do_shortcode('[contact-form-7 id="241" title="Form accessories"]'); ?>
        </div>
    </div>

	<div class="details" id="details">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php if ( get_field( 'details_title' ) ) { ?>
                        <h2><?php the_field( 'details_title' ); ?></h2>
					<?php } ?>
					<?php if ( get_field( 'details_title_label' ) ) { ?>
                        <div class="title-marker"><span class="title-marker-text"><?php the_field( 'details_title_label' ); ?></span></div>
					<?php } ?>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<?php if ( get_field( 'details_image' ) ) { ?>
                        <div class="grill-image-mobile"><img src="<?php the_field( 'details_image' ); ?>" alt="<?php the_field( 'details_image_alt' ); ?>"></div>
					<?php } ?>
					<ul class="details-list">
						<?php
						$details_item = get_field( 'details_item' );
						if($details_item) {
							foreach ( $details_item as $key => $value ) {
								?>
                                <li class="details-item" data-element="<?php echo $value['order']; ?>">
                                    <div class="details-item-title"><?php echo $value['title']; ?></div>
                                    <div class="details-item-text"><?php echo $value['text']; ?></div>
                                </li>
								<?php
							}
						}?>
					</ul>
					<div class="details-grill">
						<?php
						$grill_item = get_field( 'grill_item' );
						if($grill_item) {
							foreach ( $grill_item as $key => $value ) {
								?>
                                <div class="details-grill-element" data-element="<?php echo $key + 1; ?>"><img src="<?php echo $value['image']; ?>" alt="<?php echo $value['text']; ?>"></div>
								<?php
							}
						}?>
                    </div>
                    <div class="details-links">
                        <button class="details-links-item" data-toggle="modal" data-target="#properties">
                            <span class="details-links-icon">
                                <img src="<?php the_field( 'properties_button_icon' ) ?>" alt="<?php the_field( 'properties_button_icon_alt' ) ?>">
                            </span>
                            <span class="details-links-text"><?php the_field( 'properties_button_text' ) ?></span>
                        </button>
                        <a href="<?php the_field( 'properties_file' ) ?>" class="details-links-item" target="_blank">
                            <span class="details-links-icon">
                                <img src="<?php the_field( 'properties_link_icon' ) ?>" alt="<?php the_field( 'properties_link_icon_alt' ) ?>">
                            </span>
                            <span class="details-links-text"><?php the_field( 'properties_link_text' ) ?></span>
                        </a>
                    </div>
				</div>
			</div>
		</div>
	</div>

	<div class="interiors" id="interiors" style="background: url('<?php the_field( 'interiors_background' ); ?>') no-repeat center center; background-size: cover;">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php if ( get_field( 'interiors_title' ) ) { ?>
                        <h2><?php the_field( 'interiors_title' ); ?></h2>
					<?php } ?>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="interiors-slider">
						<?php
						$interiors_item = get_field( 'interiors_item' );
						if($interiors_item) {
							foreach ( $interiors_item as $key => $value ) {
								?>
                                <div class="interiors-slide">
                                    <a class="interiors-image" href="<?php echo $value['image_large']; ?>" data-fancybox="interiors-<?php echo $key; ?>" data-caption="<?php echo $value['name']; ?>, <?php echo $value['data']; ?>">
                                        <img src="<?php echo $value['image_small']; ?>" alt="<?php echo $value['image_small_alt']; ?>">
                                    </a>
	                                <?php
	                                $additional_images = $value['additional_images'];
	                                if($additional_images) {
		                                foreach ( $additional_images as $key_inner => $value_inner ) {
			                                ?>
                                            <a class="interiors-image-additional" href="<?php echo $value_inner['image']; ?>" data-fancybox="interiors-<?php echo $key; ?>" data-caption="<?php echo $value['name']; ?>, <?php echo $value['data']; ?>"></a>
			                                <?php
		                                }
	                                }?>
                                    <a href=""></a>
                                    <div class="interiors-name"><?php echo $value['name']; ?></div>
                                    <div class="interiors-data"><?php echo $value['data']; ?></div>
                                </div>
								<?php
							}
						}?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="order">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php if ( get_field( 'order_title_top' ) ) { ?>
                        <h2><?php the_field( 'order_title_bottom' ); ?></h2>
					<?php } ?>
					<?php if ( get_field( 'order_image' ) ) { ?>
                        <div class="order-image"><img src="<?php the_field( 'order_image' ); ?>" alt="<?php the_field( 'order_image_alt' ); ?>"></div>
					<?php } ?>
					<?php echo do_shortcode('[contact-form-7 id="226" title="Form order bottom"]'); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="video" style="background: url('<?php the_field( 'video_background' ); ?>') no-repeat center bottom; background-size: cover;">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php if ( get_field( 'video_title' ) ) { ?>
                        <h2><?php the_field( 'video_title' ); ?></h2>
					<?php } ?>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="video-slider">
						<?php
						$video_item = get_field( 'video_item' );
						if($video_item) {
							foreach ( $video_item as $key => $value ) {
								?>
                                <div class="video-slide">
                                    <a class="video-image" href="<?php echo $value['video']; ?>" data-fancybox>
                                        <img src="<?php echo $value['image']; ?>" alt="<?php echo $value['image_alt']; ?>">
                                    </a>
                                    <div class="video-text"><?php echo $value['text']; ?></div>
                                </div>
								<?php
							}
						}?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="info" id="info">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php if ( get_field( 'info_title' ) ) { ?>
                        <h2 class="text-red"><?php the_field( 'info_title' ); ?></h2>
					<?php } ?>
					<div class="info-list">
						<?php
						$info_item = get_field( 'info_item' );
						if($info_item) {
							foreach ( $info_item as $key => $value ) {
								?>
                                <div class="info-item">
                                    <div class="info-image">
                                        <img src="<?php echo $value['image']; ?>" alt="<?php echo $value['image_alt']; ?>">
                                    </div>
                                    <div class="info-title"><?php echo $value['title']; ?></div>
                                    <div class="info-text"><?php echo $value['text']; ?></div>
                                </div>
								<?php
							}
						}?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="on-map">
		<?php if ( get_field( 'map_iframe' ) ) { ?>
            <?php the_field( 'map_iframe' ); ?>
		<?php } ?>
	</div>

<?php get_footer(); ?>