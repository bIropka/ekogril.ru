var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglifyjs'),
    rename = require('gulp-rename'),
    del = require('del'),
    sass = require('gulp-sass'),
    plumber = require("gulp-plumber"),
    notify = require("gulp-notify"),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    cleanCSS = require('gulp-clean-css');

/////////////////////////////////
/////////// scss
/////////////////////////////////

gulp.task('styles', function () {
    return gulp.src('dev/scss/main.scss')
        .pipe(rename("custom-styles.scss"))
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .on("error", notify.onError(function(error) {
            return "Something happened: " + error.message;
        }))
        .pipe(autoprefixer(['last 2 version']))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('css'));
});

gulp.task('styles-prod', function () {
    return gulp.src('dev/scss/main.scss')
        .pipe(rename("custom-styles.scss"))
        .pipe(plumber())
        .pipe(sass())
        .on("error", notify.onError(function(error) {
            return "Something happened: " + error.message;
        }))
        .pipe(autoprefixer(['last 2 version']))
        .pipe(cleanCSS())
        .pipe(gulp.dest('css'));
});

/////////////////////////////////
/////////// scripts
/////////////////////////////////

gulp.task('scripts', function() {
    return gulp.src([
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'node_modules/slick-carousel/slick/slick.min.js',
        'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js',
        'dev/js/maskedinput.js',
        'dev/js/custom-scripts.js'
    ])
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js'));
});

gulp.task('jquery', function() {
    return gulp.src([
        'node_modules/jquery/dist/jquery.min.js'
    ])
        .pipe(gulp.dest('js'))
});

/////////////////////////////////
/////////// watch
/////////////////////////////////

gulp.task('watch', [ 'build' ], function() {
    gulp.watch('dev/scss/**/*.scss', ['styles']);
    gulp.watch('dev/markup/**/*.pug', ['markup']);
    gulp.watch(['dev/js/**/*.js', 'dev/libs/**/*.js'], ['scripts']);
});

/////////////////////////////////
/////////// clean
/////////////////////////////////

gulp.task('clean', function() {
    return del.sync(['css', 'js']);
});

/////////////////////////////////
/////////// build
/////////////////////////////////

gulp.task('build', [ 'styles', 'jquery', 'scripts' ]);

/////////////////////////////////
/////////// prod
/////////////////////////////////

gulp.task('prod', [ 'styles-prod', 'jquery', 'scripts' ]);

/////////////////////////////////
/////////// default
/////////////////////////////////
gulp.task('default', ['watch']);
