$(window).ready(function () {

    var headerTop = $('.header'),
        wrapper = $('.wrapper'),
        headerSticky = $('.header-sticky');

    setTimeout(function () {
        wrapper.animate({opacity: 1}, 500);
    }, 1000);

    /********************************************
     ******************* header
     ********************************************/

    $('.burger').click(function () {
        $(this).toggleClass('active');
        $('.header-nav').slideToggle(300);
    });

    $('.header-nav a[href^="#"]').click(function() {

        $('.header-nav a[href^="#"]').removeClass('active');
        $(this).addClass('active');

        var target = $(this).attr('href');

        if(target === '#home') {
            $('html, body').animate({scrollTop: 0}, 1000);
        } else {
            $('html, body').animate({scrollTop: $(target).offset().top - 58}, 1000);
        }

        return false;

    });

    /*******************************************
     ******************* scroll, resize
     ********************************************/

    if($(window).width() >= 1230) {

        if ($(window).scrollTop() > parseInt(headerTop.css('height'))) {
            headerSticky.addClass('fixed');
            wrapper.css('padding', '50px 0 0');
        } else {
            headerSticky.removeClass('fixed');
            wrapper.css('padding', '0px');
        }

    }

    $('.header-nav a').removeClass('active');
    if($(window).scrollTop() > $('#info').offset().top - 200) {
        $('a[href="#info"]').addClass('active');
    } else if($(window).scrollTop() > $('#interiors').offset().top - 200) {
        $('a[href="#interiors"]').addClass('active');
    } else if($(window).scrollTop() > $('#details').offset().top - 200) {
        $('a[href="#details"]').addClass('active');
    } else if($(window).scrollTop() > $('#advantages').offset().top - 200) {
        $('a[href="#advantages"]').addClass('active');
    } else if($(window).scrollTop() > $('#ways').offset().top - 200) {
        $('a[href="#ways"]').addClass('active');
    } else if($(window).scrollTop() > $('#about').offset().top - 100) {
        $('a[href="#about"]').addClass('active');
    } else {
        $('a[href="#home"]').addClass('active');
    }

    $(window).scroll(function() {

        if($(window).width() >= 1230) {

            if ($(window).scrollTop() > parseInt(headerTop.css('height'))) {
                headerSticky.addClass('fixed');
                wrapper.css('padding', '50px 0 0');
            } else {
                headerSticky.removeClass('fixed');
                wrapper.css('padding', '0px');
            }

        }

        $('.header-nav a').removeClass('active');
        if($(window).scrollTop() > $('#info').offset().top - 200) {
            $('a[href="#info"]').addClass('active');
        } else if($(window).scrollTop() > $('#interiors').offset().top - 200) {
            $('a[href="#interiors"]').addClass('active');
        } else if($(window).scrollTop() > $('#details').offset().top - 200) {
            $('a[href="#details"]').addClass('active');
        } else if($(window).scrollTop() > $('#advantages').offset().top - 200) {
            $('a[href="#advantages"]').addClass('active');
        } else if($(window).scrollTop() > $('#ways').offset().top - 200) {
            $('a[href="#ways"]').addClass('active');
        } else if($(window).scrollTop() > $('#about').offset().top - 100) {
            $('a[href="#about"]').addClass('active');
        } else {
            $('a[href="#home"]').addClass('active');
        }

    });

    $(window).resize(function() {

        if($(window).width() < 1230) {
            headerSticky.removeClass('fixed');
            wrapper.css('padding', '50px 0 0');
        } else {
            wrapper.css('padding', '0px');
        }

    });

    /*******************************************
     ******************* advantages
     ********************************************/

    $('.advantages-item').click(function(event) {

        if($(event.target).closest('.advantages-subitem').length === 0 || $(event.target).hasClass('advantages-close')) {
            $(this).find('.advantages-subimage').html($(this).find('.advantages-image').html());
            $(this).find('.advantages-subtitle').html($(this).find('.advantages-title').html());
            $(this).toggleClass('active');
            $(this).siblings('.advantages-item').removeClass('active');
        }

    });

    /*******************************************
     ******************* slider
     ********************************************/

    var sliderTop = $('.top-slider'),
        sliderInteriors = $('.interiors-slider'),
        sliderVideo = $('.video-slider');

    if (sliderTop.length > 0) {

        sliderTop.slick({
            dots: true,
            fade: true
        });

    }

    if (sliderInteriors.length > 0) {
        sliderInteriors.slick({
            slidesToShow: 4,
            responsive: [
                {
                    breakpoint: 1230,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }

    if (sliderVideo.length > 0) {
        sliderVideo.slick({
            slidesToShow: 2,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }

    /*******************************************
     ******************* details
     ********************************************/

    $('.details-item-title').hover(function() {

        var element = $(this).parents('.details-item').attr('data-element');
        $(this).parents('.details-item').toggleClass('active');
        $(this).parents('.details-item').siblings('.details-item').toggleClass('unactive');

        $('.details-grill-element[data-element=' + element + ']').toggleClass('active');
        $('.details-grill-element:not([data-element=' + element + '])').toggleClass('unactive');

    });

    /*******************************************
     ******************* modal
     ********************************************/

    $('.custom-dropdown').click(function(event) {
        event.preventDefault();
        if($(event.target).closest('.custom-dropdown-menu').length === 0) {
            $(this).toggleClass('active');
        }
    });

    $('.custom-dropdown-menu li').click(function() {
        if(!$(this).hasClass('active')) {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $(this).parent().siblings('.custom-dropdown-value').text($(this).text());
            $(this).parents('.custom-dropdown').find('input').val($(this).text());
            $(this).parents('.custom-dropdown').removeClass('active');
        }
    });

    document.addEventListener( 'wpcf7mailsent', function(event) {
        var form = $(event.target);
        if (form.find('#top-submit').length) {
            $('#top-mail-sent-success').modal('show');
        } else if (form.find('#order-top-submit').length || form.find('#order-bottom-submit').length) {
            $('#order-mail-sent-success').modal('show');
        } else if (form.find('#accessories-submit').length) {
            $('#accessories-mail-sent-success').modal('show');
        } else if (form.find('#callback-submit').length) {
            $('#callback').modal('hide');
            $('#callback-mail-sent-success').modal('show');
        }

    }, false );

    $('input[type="tel"]').each(function(index, elem) {
        $(elem).mask("+7(999) 999 99 99");
    });

    /*******************************************
     ******************* accessories
     ********************************************/

    var accessories = [],
        accessoriesEl = $('input.form-accessories-checkbox'),
        accessoriesVal = 'Без аксессуаров';

    accessoriesEl.each(function(i, el) {
        if($(el).val()) {
            accessories.push($(el).val());
        }
        $(el).val(accessoriesVal);

    });

    accessoriesEl.change(function() {
        accessoriesVal = '';
        $('.form-accessories-checkbox').val(accessoriesVal);
        $(this).toggleClass('active');
        accessoriesEl.each(function(i, el) {
            if($(el).hasClass('active')) {
                accessoriesVal += accessories[i] + '  ';
            }
        });
        if(accessoriesVal === '') accessoriesVal = 'Без аксессуаров';
        $('.form-accessories-checkbox').val(accessoriesVal);
    });

    $('input').keyup(function() {
        if($(this).val() !== '') {
            $(this).siblings('span[class*="not-valid-tip"]').css('display', 'none');
        } else {
            $(this).siblings('span[class*="not-valid-tip"]').css('display', 'block');
        }
    });


});
