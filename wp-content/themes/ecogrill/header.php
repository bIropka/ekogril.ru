<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <main>
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo get_template_directory_uri();?>/favicon.ico" type="image/x-icon" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="wrapper site">

    <header id="home">
        <div class="header">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-sm-6 col-xl-3 order-sm-1 order-xl-1">
                        <div class="logo">
	                        <?php if ( get_field( 'logo_icon_image' ) ) { ?>
                                <div class="logo-small"><img src="<?php the_field( 'logo_icon_image' ); ?>" alt="<?php the_field( 'logo_icon_alt' ); ?>"></div>
	                        <?php } ?>
	                        <?php if ( get_field( 'logo_text_image' ) ) { ?>
                                <div class="logo-large"><img src="<?php the_field( 'logo_text_image' ) ?>" alt="<?php the_field( 'logo_text_alt' ) ?>"></div>
                            <?php } ?>
	                        <?php if ( get_field( 'header_tagline_text_black' ) ) { ?>
                                <h1 class="logo-text"><?php the_field( 'header_tagline_text_black' ) ?> <span class="text-red"><?php the_field( 'header_tagline_text_red' ) ?></span></h1>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-xl-3 order-sm-3 order-xl-2">
                        <div class="header-data">
	                        <?php if ( get_field( 'header_address_text' ) ) { ?>
                                <div class="header-address"><?php the_field( 'header_address_text' ) ?></div>
	                        <?php } ?>
	                        <?php if ( get_field( 'opening_hours_text_black' ) ) { ?>
                                <div class="header-hours"><?php the_field( 'opening_hours_text_black' ) ?> <span class="text-red"><?php the_field( 'opening_hours_text_red' ) ?></span></div>
	                        <?php } ?>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-xl-3 order-sm-4 order-xl-3">
                        <div class="header-data">
	                        <?php if ( get_field( 'phone_number' ) ) { ?>
                                <a class="header-phone" href="tel:<?php echo preg_replace("/[^\d+]/", '', get_field('phone_number')); ?>"><?php the_field( 'phone_number' ) ?></a>
	                        <?php } ?>
	                        <?php if ( get_field( 'email_address' ) ) { ?>
                                <div class="header-email"><?php the_field( 'header_email_text' ) ?> <a href="mailto:<?php the_field( 'email_address' ) ?>" class="text-red"><?php the_field( 'email_address' ) ?></a></div>
	                        <?php } ?>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-xl-3 order-sm-2 order-xl-4 text-xl-right">
	                    <?php if ( get_field( 'header_button_text' ) ) { ?>
                            <button class="button button-decor" data-toggle="modal" data-target="#callback">
                                <span class="button-inner"><?php the_field( 'header_button_text' ) ?></span>
                            </button>
	                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-sticky">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="burger">
                            <div class="burger-top"></div>
                            <div class="burger-center"></div>
                            <div class="burger-bottom"></div>
                        </div>
	                    <?php
	                    wp_nav_menu( array(
		                    'menu_class' => 'menu',
		                    'container'=>'nav',
		                    'container_class' => 'header-nav'
	                    ) );
	                    ?>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <main>