<?php

register_nav_menus( array(
	'top' => 'Main menu'
) );

function scripts() {

	// Load our main stylesheet.
	wp_enqueue_style( 'add-styles', get_template_directory_uri() . '/assets/css/custom-styles.css' );

	// Load jquery script.
	wp_enqueue_script( 'add-jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', '', '', true );
	// Load our main scripts.
	wp_enqueue_script( 'add-scripts', get_template_directory_uri() . '/assets/js/scripts.min.js', array('add-jquery'), '', true );

}

add_action( 'wp_enqueue_scripts', 'scripts' );

define( 'WPCF7_AUTOP', false );